<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LARAVEL, VUEJS AND VUETIFY</title>

    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>

        .footer{

            position: fixed;

            bottom: 0px;

            width: 100%;

        }

        .material-icons{

            display: none;

        }

    </style>

</head>

<body>

    <div id="app">

        <v-app light v-cloak>

            <v-toolbar class="white">

                <v-toolbar-title><h4>JUNRIL PATEÑO TEST</h4></v-toolbar-title>

            </v-toolbar>

            <v-content>

                <section>

                    <v-layout

                        column

                        wrap

                        class="my-5"

                        align-center

                    >

                        <v-flex xs12>

                            <v-container grid-list-xl>

                                <v-layout row wrap align-center>

                                    <v-tabs

                                        color="cyan"

                                        dark

                                        slider-color="yellow"

                                        class="col-lg-12"

                                    >

                                        <v-tab :key="1" ripple>Step 1</v-tab>

                                        <v-tab :key="2" ripple>Step 2</v-tab>

                                        <v-tab :key="3" ripple>Step 3</v-tab>


                                        <v-tab-item :key="1">

                                            <v-card flat>

                                                <v-card-text>lorem ipsum text</v-card-text>

                                            </v-card>

                                        </v-tab-item>

                                        <v-tab-item :key="2">

                                            <v-card flat>

                                                <v-card-text>

                                                    <v-flex xs12 sm5 md5>

                                                        <v-select

                                                            :items="items"

                                                            label="Select option to execute!"

                                                            v-on:change="hideImage"

                                                        >

                                                        </v-select>

                                                    </v-flex>

                                                    <v-flex xs12 sm12 md12>

                                                        <v-img

                                                            src="https://picsum.photos/350/165?random"

                                                            height="450"

                                                            class="grey darken-4"

                                                            v-if="image"
                                                        
                                                        >

                                                        </v-img>

                                                    </v-flex>

                                                    <v-btn color="success" v-if="undo" v-on:click="showImage">Undo Image</v-btn>

                                                </v-card-text>

                                            </v-card>

                                        </v-tab-item>

                                        <v-tab-item :key="3">

                                            <v-card flat>

                                                <v-card-text>

                                                    <v-flex class="col-lg-6">

                                                        <v-text-field label="Enter your name" v-model="fullName" ref="fullname"

                                                        ></v-text-field>

                                                    </v-flex>

                                                    <v-flex class="col-lg-6">

                                                        <v-menu

                                                            :close-on-content-click="false"

                                                            v-model="menu2"

                                                            :nudge-right="40"

                                                            lazy

                                                            transition="scale-transition"

                                                            offset-y

                                                            full-width

                                                            min-width="290px"

                                                        >

                                                            <v-text-field

                                                                slot="activator"

                                                                v-model="dateBirth"

                                                                label="Please select you birth date!"

                                                                prepend-icon="event"

                                                                readonly 

                                                                required

                                                            >
                                                        
                                                            </v-text-field>

                                                            <v-date-picker v-model="dateBirth" @input="menu2 = false"></v-date-picker>

                                                        </v-menu>

                                                    </v-flex>

                                                    <v-btn color="primary" v-on:click="submitForm"> Submit </v-btn>

                                                </v-card-text>

                                            </v-card>

                                        </v-tab-item>

                                    </v-tabs>

                                </v-layout>

                            </v-container>

                        </v-flex>

                    </v-layout>

                </section>

                <v-dialog v-model="dialog" width="500">

                    <v-card>

                        <v-card-title class="headline grey lighten-2" primary-title>

                            INFORMATION

                        </v-card-title>

                        <v-card-text>

                            Name : @{{ your_full_name }}

                        </v-card-text>

                        <v-card-text>

                            Birthdate : @{{ your_birth_date }}

                        </v-card-text>

                        <v-divider></v-divider>

                        <v-card-actions>

                            <v-spacer></v-spacer>

                            <v-btn color="primary" flat @click="dialog = false">

                                Close

                            </v-btn>

                        </v-card-actions>

                    </v-card>

                </v-dialog>

                <v-footer class="blue darken-2 footer">

                    <v-layout row wrap align-center>

                        <v-flex xs12>

                            <div class="white--text ml-3">

                                POWERED BY LARAVEL, VUEJS AND VUETIFY CUSTOM BY JUNRIL PATEÑO

                            </div>

                        </v-flex>

                    </v-layout>

                </v-footer>

            </v-content>

        </v-app>

    </div>

    <script type="text/javascript" src="js/vue.min.js"></script>

    <script type="text/javascript" src="js/app.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>

    <script>

        new Vue({ 

            el: '#app',

            data: {

                fullName:'',

                image: true,

                undo: false,

                items: ['Hide'],

                dateBirth: '',

                menu: false,

                modal: false,

                menu2: false,

                dialog: false,

                your_full_name: '',

                your_birth_date: ''

            },

            methods: {

                hideImage() {

                    this.image = false;

                    this.undo = true;

                },

                showImage() {

                    this.image = true;

                    this.undo = false;

                },

                submitForm() {

                    var name = this.fullName;

                    var date = this.dateBirth;

                        if( name == '') {

                            alert( " Name is required!   ");

                            return false;

                        }

                        if( date == '') {

                            alert( " Birthdate is required! ");

                            return false;

                        }

                        this.your_full_name = name;

                        this.your_birth_date = date;

                        this.dialog = true;

                },

            }

        })

    </script>

</body>

</html>
